use super::grid_item;
use gtk::prelude::*;
use relm::{init, Relm, Update, Widget};
use std::convert::TryFrom;
use xdg_menu::{list_application_objects, KValue, Line};

enum RenderError {
    PE(grid_item::ParamError),
    N,
}

impl std::convert::From<grid_item::ParamError> for RenderError {
    fn from(e: grid_item::ParamError) -> Self {
        Self::PE(e)
    }
}
impl std::convert::From<()> for RenderError {
    fn from(_: ()) -> Self {
        Self::N
    }
}
impl std::convert::From<std::option::NoneError> for RenderError {
    fn from(_: std::option::NoneError) -> Self {
        Self::N
    }
}

#[derive(relm_derive::Msg)]
pub enum Msg {
    Populate,
    Launch,
    Filter(String),
}

pub struct ApplicationGrid {
    root_element: gtk::FlowBox,
    items: Vec<(xdg_menu::DesktopEntry, relm::Component<grid_item::GridItem>)>,
}

impl Update for ApplicationGrid {
    type Model = ();
    type ModelParam = ();
    type Msg = Msg;

    fn model(_: &Relm<Self>, _: ()) -> () {}

    fn update(&mut self, event: Msg) {
        match event {
            Msg::Populate => {
                let (mut apps, _errors) = list_application_objects();

                for a in apps.drain(..) {
                    let _r: Result<(), RenderError> = try {
                        let i = init::<grid_item::GridItem>(grid_item::Param::try_from(&a)?)?;
                        self.items.push((a, i));
                        self.root_element.add(self.items.last()?.1.widget());
                    };
                }

                self.root_element.show_all();
            }
            Msg::Launch => {
                println!("Launch Event");
                let item = self
                    .items
                    .iter()
                    .find(|i| i.1.widget().get_parent().unwrap().get_visible());
                match item {
                    Some(i) => {println!("Executing"); i.1.emit(grid_item::Msg::Exec)},
                    None => {}
                }
            }
            Msg::Filter(filter) => {
                for app in self.items.iter() {
                    app.1.stream().emit(grid_item::Msg::Filter(filter.clone()));
                }
            }
        }
    }
}

impl Widget for ApplicationGrid {
    type Root = gtk::FlowBox;

    fn root(&self) -> Self::Root {
        self.root_element.clone()
    }

    fn view(_relm: &Relm<Self>, _model: Self::Model) -> Self {
        let rm = gtk::FlowBox::new();
        rm.show_all();
        let mut grid = ApplicationGrid {
            root_element: rm,
            items: Vec::new(),
        };
        grid.update(Msg::Populate);
        grid
    }
}
