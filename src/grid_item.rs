use gtk::prelude::*;
use relm::{Relm, Update, Widget};
use std::convert::TryFrom;
use xdg_menu::{line::Exec, DesktopEntry, KValue, Line};

use glib::spawn_command_line_async;

#[derive(Debug)]
pub struct Model {
    name: String,
    exec: Exec,
    icon: Option<gtk::Image>,
}

#[derive(Debug)]
pub struct GridItem {
    model: Model,
    root_element: gtk::ListBoxRow,
}

pub enum ParamError {
    NoneError(std::option::NoneError),
    ExecError(xdg_menu::line::Error),
    TypeError,
}

impl From<std::option::NoneError> for ParamError {
    fn from(e: std::option::NoneError) -> Self {
        Self::NoneError(e)
    }
}

impl From<xdg_menu::line::Error> for ParamError {
    fn from(e: xdg_menu::line::Error) -> Self {
        Self::ExecError(e)
    }
}

#[derive(Debug)]
pub struct Param(String, Exec, Option<String>);

impl TryFrom<&DesktopEntry> for Param {
    type Error = ParamError;
    fn try_from(entry: &DesktopEntry) -> Result<Self, Self::Error> {
        let de = entry.groups.get("Desktop Entry")?;
        let name = match de.get("Name")?.clone() {
            Line::KeyVal(a) => match a.value {
                KValue::String(b) => Ok(b),
                _ => Err(ParamError::TypeError),
            },
            _ => Err(ParamError::TypeError),
        }?;

        let exec: Exec = Exec::try_from(entry.clone())?;

        let icon = de
            .get("Icon")
            .map(|i| match i {
                Line::KeyVal(a) => match a.value.clone() {
                    KValue::String(b) => Some(b),
                    _ => None,
                },
                _ => None,
            })
            .flatten();

        Ok(Self(name, exec, icon))
    }
}

#[derive(relm_derive::Msg)]
pub enum Msg {
    Exec,
    Filter(String),
}

impl Update for GridItem {
    type Model = Model;
    type ModelParam = Param;
    type Msg = Msg;

    fn model(_: &Relm<Self>, d: Self::ModelParam) -> Model {
        let icon: Option<gtk::Image> = try {
            let theme = gtk::IconTheme::get_default()?;

            let icon_buf =
                theme.load_icon(&d.2.clone()?.clone(), 32, gtk::IconLookupFlags::empty());
            let icon_buf = match icon_buf {
                Ok(i) => Ok(i),
                Err(_) => Err(std::option::NoneError),
            }??;

            let icon_buf = icon_buf.scale_simple(32, 32, gdk_pixbuf::InterpType::Bilinear)?;
            gtk::Image::new_from_pixbuf(Some(&icon_buf))
        };

        Model {
            name: d.0,
            exec: d.1,
            icon,
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::Exec => {
                println!(
                    "Executing {}, with Executable: {:?}",
                    self.model.name, self.model.exec
                );
                let _ = glib::spawn_command_line_async(self.model.exec.command.clone());
            }
            Msg::Filter(filter) => {
                let mut name = self.model.name.clone();
                name.make_ascii_lowercase();
                let mut filter = filter.clone();
                filter.make_ascii_lowercase();

                if name.contains(&filter) {
                    self.root_element.get_parent().unwrap().show();
                } else {
                    self.root_element.get_parent().unwrap().hide();
                }
            }
        }
    }
}

impl Widget for GridItem {
    type Root = gtk::ListBoxRow;

    fn root(&self) -> Self::Root {
        self.root_element.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        let rm = gtk::ListBoxRow::new();
        let button = gtk::Button::new();
        rm.add(&button);

        let container = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        button.add(&container);

        match model.icon.clone() {
            Some(i) => container.add(&i),
            None => {}
        }

        let label = {
            let name: &str = &model.name;
            gtk::Label::new(Some(name))
        };
        container.add(&label);

        connect!(relm, button, connect_clicked(_), Msg::Exec);

        rm.show_all();

        GridItem {
            root_element: rm,
            model,
        }
    }
}
