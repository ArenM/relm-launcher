#![feature(try_blocks)]
#![feature(try_trait)]

extern crate relm;
extern crate relm_attributes;
extern crate relm_derive;

use std::convert::TryFrom;

use gtk::prelude::*;
use gtk::Orientation::Vertical;
use launcher::application_grid::{ApplicationGrid, Msg as ApplicationGridMsg};
use relm::{connect, connect_stream, Relm, Update, Widget};
use relm_derive::Msg;

use self::HeaderMsg::Update as HeaderUpdate;

use gtk::Inhibit;

pub struct Header {
    rm: gtk::HeaderBar,
    entry: gtk::Entry,
}

#[derive(Msg)]
pub enum HeaderMsg {
    Update(String),
    Focus,
}

impl Update for Header {
    type Model = ();
    type Msg = HeaderMsg;
    type ModelParam = ();

    fn model(_: &Relm<Self>, _: ()) -> () {}
    fn update(&mut self, msg: Self::Msg) {
        match msg {
            HeaderMsg::Update(_) => {}
            HeaderMsg::Focus => self.entry.grab_focus_without_selecting(),
        }
    }
}
impl Widget for Header {
    type Root = gtk::HeaderBar;

    fn root(&self) -> Self::Root {
        self.rm.clone()
    }

    fn view(relm: &relm::Relm<Self>, _model: ()) -> Self {
        let rm = gtk::HeaderBar::new();
        rm.set_show_close_button(true);

        let entry = gtk::Entry::new();
        //let _ = entry.set_property("expand", &true);
        entry.set_placeholder_text(Some("Search"));
        rm.set_custom_title(Some(&entry));

        let stream = relm.stream().clone();
        entry.connect_changed(move |e| {
            let filter = e.get_buffer().get_text();
            stream.emit(HeaderMsg::Update(filter));
        });

        rm.show_all();
        Self { rm, entry }
    }
}

#[derive(Msg)]
pub enum Msg {
    Quit,
    Filter(String),
    Launch,
    Escape,
    None,
}

pub struct Win {
    rm: gtk::Window,
    app_grid: relm::Component<ApplicationGrid>,
    header_bar: relm::Component<Header>,
}

impl Update for Win {
    type Model = ();
    type Msg = Msg;
    type ModelParam = ();

    fn model(_: &relm::Relm<Self>, _: ()) -> () {}
    fn update(&mut self, evnet: Msg) {
        match evnet {
            Msg::Filter(filter) => {
                self.app_grid
                    .emit(ApplicationGridMsg::Filter(filter.clone()));
            }
            Msg::Quit => gtk::main_quit(),
            Msg::Escape => gtk::main_quit(),
            _ => {},
        }
    }
}

impl Widget for Win {
    type Root = gtk::Window;

    fn root(&self) -> Self::Root {
        self.rm.clone()
    }

    fn view(relm: &relm::Relm<Self>, _model: ()) -> Self {
        let rm = gtk::Window::new(gtk::WindowType::Toplevel);
        let b = gtk::Box::new(Vertical, 0);
        rm.add(&b);

        let hb = relm::init::<Header>(()).unwrap();
        let hbs = hb.stream();
        connect_stream!(hbs@HeaderUpdate(ref filter), relm.stream(), Msg::Filter(filter.clone()));
        b.add(hb.widget());

        let none: Option<&gtk::Adjustment> = None;
        let sw = gtk::ScrolledWindow::new(none, none);
        sw.set_property_expand(true);
        b.add(&sw);

        let ag = relm::init::<ApplicationGrid>(()).unwrap();
        sw.add(ag.widget());
        {
            let ags = ag.stream();
            let s = relm.stream();
            // connect_stream!(ags@ApplicationGridMsg::Launch, relm.stream(), Msg::Launch);
            connect_stream!(s@Msg::Launch, ags, ApplicationGridMsg::Launch);
        }

        connect!(
            relm,
            rm,
            connect_delete_event(_, _),
            return (Msg::Quit, Inhibit(false))
        );
        // connect!(relm, rm, connect_key_press_event(_,_), return (Msg::None, {println!("to inhibit"); Inhibit(false)}));
        {
            let stream = relm.stream().clone();
            let header_stream = hb.stream().clone();
            rm.connect_key_press_event(move |_target, e| {
                match char::try_from(e.get_keyval()) {
                    Ok(k) => {
                        // Enter Key
                        if k == '\u{FF0D}' {
                            stream.emit(Msg::Launch)
                        }
                        // Escape Key
                        if k == '\u{FF1B}' {
                            stream.emit(Msg::Escape)
                        }
                        // println!("{:x}", k as u32);
                    }
                    Err(_) => {}
                };
                // TODO: This could cause a race where key press events are ignored
                header_stream.emit(HeaderMsg::Focus);
                Inhibit(false)
            });
        }

        rm.show_all();
        Self {
            rm,
            app_grid: ag,
            header_bar: hb,
        }
    }
}

fn main() {
    Win::run(()).expect("Window Failed");
}
