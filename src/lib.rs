#![feature(try_trait)]
#![feature(try_blocks)]

#[macro_use]
extern crate relm;
extern crate relm_derive;

pub mod application_grid;
mod grid_item;
